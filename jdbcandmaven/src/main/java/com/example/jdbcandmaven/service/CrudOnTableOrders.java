package com.example.jdbcandmaven.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.example.jdbcandmaven.domain.ClientDetails;
import com.example.jdbcandmaven.domain.Order;
import com.example.jdbcandmaven.domain.OrderItem;

public class CrudOnTableOrders {
	
		private PreparedStatement addNewOrder;
		private PreparedStatement viewOrders;
		private PreparedStatement deleteOrder;
		
		private String addNewOrderSql = "INSERT INTO Orders (id, client, deliveryAddress, items) VALUES (1, '?', '?', '?')";
		private String viewOrdersSql = "SELECT id, client, items FROM Orders";
		private String deleteOrderSql = "DELETE FROM Orders";
		
		private Connection connection;
		
		public CrudOnTableOrders() {
			setConnection();
		}
		
		private void setConnection() {
		this.connection = new ConnectWithDatabase().getConnection();
		}
		
		public boolean addNewOrder() {
			boolean isTableCreate = false;
			try{
			addNewOrder = this.connection.prepareStatement(addNewOrderSql);
			int counter = 0;
			counter = addNewOrder.executeUpdate();
			if (counter > 0) isTableCreate = true;
			} catch(SQLException e) {
				e.printStackTrace();
			} finally {
				return isTableCreate;
			}
		}
		
		public boolean viewOrders() {
			boolean isTableInView = false;
			try {
			viewOrders = this.connection.prepareStatement(viewOrdersSql);
			ResultSet rs = viewOrders.executeQuery();
			while(rs.next()) {
				Order domainOrder = new Order();
				
				domainOrder.setId(rs.getLong("id"));
				
				ClientDetails domainClientDetails = new ClientDetails();
				domainClientDetails.setName(rs.getString("client"));
				domainOrder.setClient(domainClientDetails);
				
				OrderItem domainOrderItem = new OrderItem();
				domainOrderItem.setName(rs.getString("items"));
				List<OrderItem> domainItemsList = new ArrayList();
				domainItemsList.add(domainOrderItem);
				domainOrder.setItems(domainItemsList);
				
				if(domainOrder.getId()==1 
					|| domainOrder.getClient().getName().equals("?")
					|| domainOrder.getItems().get(0).equals("?")) {
					isTableInView = true;
				}
				
			}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				return isTableInView;

			}
		}
		
		public boolean deleteOrder() {
			boolean isTableDelete = false;
			try{
			deleteOrder = this.connection.prepareStatement(deleteOrderSql);
			int counter = 0;
			counter = deleteOrder.executeUpdate();
			if (counter > 0) isTableDelete = true;
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				return isTableDelete;

			}
		}
				
}
