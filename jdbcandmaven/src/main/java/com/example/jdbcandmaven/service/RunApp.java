package com.example.jdbcandmaven.service;

public class RunApp {

	public static void main(String[] args) {
		CreateTableOrder createTableOrder = new CreateTableOrder();
		createTableOrder.createTable();
		
		CrudOnTableOrders crudOnTableOrders = new CrudOnTableOrders();
		crudOnTableOrders.addNewOrder();
		crudOnTableOrders.viewOrders();
		crudOnTableOrders.deleteOrder();

	}

}
