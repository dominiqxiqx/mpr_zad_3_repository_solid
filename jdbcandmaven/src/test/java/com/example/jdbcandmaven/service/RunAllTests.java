package com.example.jdbcandmaven.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ConnectWithDatabaseTest.class, CreateTableOrderTest.class, CrudOnTableOrderTest.class})
public class RunAllTests {

}
