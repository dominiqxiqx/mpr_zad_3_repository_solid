package com.example.jdbcandmaven.service;

import static org.junit.Assert.*;

import org.junit.Test;

public class CrudOnTableOrderTest {
	
		CrudOnTableOrders crudOnTableOrders = new CrudOnTableOrders();
		
		
		@Test
		public void addNewOrderTest() {
			assertTrue(crudOnTableOrders.addNewOrder());
		}
		

		@Test
		public void viewOrdersTest() {
			assertTrue(crudOnTableOrders.viewOrders());
		}
		
		@Test
		public void deleteOrderTest() {
			assertTrue(crudOnTableOrders.deleteOrder());
		}
}
