package com.example.jdbcandmaven.repository;

import com.example.jdbcandmaven.service.ConnectWithDatabase;
import com.example.jdbcandmaven.service.CreateTableOrder;
import com.example.jdbcandmaven.service.AddNewOrder;
import com.example.jdbcandmaven.service.DeleteOrder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.example.jdbcandmaven.domain.Address;
import com.example.jdbcandmaven.domain.ClientDetails;
import com.example.jdbcandmaven.domain.Order;
import com.example.jdbcandmaven.domain.OrderItem;
import com.example.jdbcandmaven.interfaces.OrderRepository;


public class HsqlOrderRepository implements OrderRepository {
					
	Connection connection;
	
	private String selectByIdSql ="SELECT * FROM Orders WHERE id=?";
	private String updateSql = "UPDATE Orders SET (client,deliveryAddress)=(?,?) WHERE id=?";
	private String selectByLoginSql ="SELECT * FROM person WHERE client=?";
	private String selectByItemNameSql ="SELECT * FROM person WHERE items=?";



	private PreparedStatement selectById;
	private PreparedStatement update;
	private PreparedStatement selectByLogin;
	private PreparedStatement selectByItem;

	
	
	public HsqlOrderRepository () {
		this.connection = setConnection();
		createTableOrder();
	}
	
	private Connection setConnection() {
		ConnectWithDatabase connectWithDatabase = new ConnectWithDatabase();
		return connectWithDatabase.getConnection();
	}
	
	public void createTableOrder () {
		CreateTableOrder createTableOrder = new CreateTableOrder();
		createTableOrder.createTable();
	}
	
	public Order withId(int id) {
		
		Order result = null;
		List <OrderItem> items = new ArrayList<OrderItem>();
		
		try {
			
		selectById = connection.prepareStatement(selectByIdSql);
		selectById.setInt(1, id);
		ResultSet rs = selectById.executeQuery();
		while(rs.next()) {
			Order order = new Order();
			
			order.setId(rs.getInt("id"));
			
			ClientDetails clientDetails = new ClientDetails();
			clientDetails.setName(rs.getString("client"));
			order.setClient(clientDetails);
			
			Address address = new Address();
			address.setStreet(rs.getString("deliveryAddress"));
			order.setDelivaryAddress(address);
			
			OrderItem orderItem = new OrderItem();
			orderItem.setName(rs.getString("items"));
			items.add(orderItem);
			order.setItems(items);
			
			result = order;
			break;
		}
			
		
		} catch(SQLException ex){
			ex.printStackTrace(); 
			}
		
		return result;
	}
	
	public void add(Order order) {
		AddNewOrder addNewOrder = new AddNewOrder();
		addNewOrder.newOrder();
	}
	
	public void remove (Order order) {
		DeleteOrder deleteOrder = new DeleteOrder();
		deleteOrder.deleteOrder();
	}
	
	public void modify (Order order) {
		
		try{
			
			update = connection.prepareStatement(updateSql);
			
			update.setString(1, order.getClient().getLogin());
			update.setString(2, order.getDelivaryAddress().getCity());
			update.setInt(3, (int)order.getId());
			update.executeUpdate();

		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		
	}
	
	public List<Order> withLogin (String login) {
		
		List<Order> result = new ArrayList();
		List <OrderItem> items = new ArrayList<OrderItem>();
		
		try {
			
		selectByLogin = connection.prepareStatement(selectByLoginSql);
		selectByLogin.setString(1, login);
		ResultSet rs = selectByLogin.executeQuery();
		while(rs.next()) {
			Order order = new Order();
			
			order.setId(rs.getInt("id"));
			
			ClientDetails clientDetails = new ClientDetails();
			clientDetails.setName(rs.getString("client"));
			order.setClient(clientDetails);
			
			Address address = new Address();
			address.setStreet(rs.getString("deliveryAddress"));
			order.setDelivaryAddress(address);
			
			OrderItem orderItem = new OrderItem();
			orderItem.setName(rs.getString("items"));
			items.add(orderItem);
			order.setItems(items);
			
			result.add(order);
			break;
		}
			
		
		} catch(SQLException ex){
			ex.printStackTrace(); 
			}
		
		return result;
	}
	
public List<Order> withItemName (String itemName) {
		
		List<Order> result = new ArrayList();
		List <OrderItem> items = new ArrayList<OrderItem>();
		
		try {
			
		selectByItem = connection.prepareStatement(selectByItemNameSql);
		selectByItem.setString(1, itemName);
		ResultSet rs = selectByItem.executeQuery();
		while(rs.next()) {
			Order order = new Order();
			
			order.setId(rs.getInt("id"));
			
			ClientDetails clientDetails = new ClientDetails();
			clientDetails.setName(rs.getString("client"));
			order.setClient(clientDetails);
			
			Address address = new Address();
			address.setStreet(rs.getString("deliveryAddress"));
			order.setDelivaryAddress(address);
			
			OrderItem orderItem = new OrderItem();
			orderItem.setName(rs.getString("items"));
			items.add(orderItem);
			order.setItems(items);
			
			result.add(order);
			break;
		}
			
		
		} catch(SQLException ex){
			ex.printStackTrace(); 
			}
		
		return result;
	}




}
