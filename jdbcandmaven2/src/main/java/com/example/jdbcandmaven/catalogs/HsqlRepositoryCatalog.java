package com.example.jdbcandmaven.catalogs;

import com.example.jdbcandmaven.interfaces.OrderRepository;
import com.example.jdbcandmaven.interfaces.RepositoryCatalog;
import com.example.jdbcandmaven.repository.HsqlOrderRepository;

public class HsqlRepositoryCatalog implements RepositoryCatalog {

	public OrderRepository orders() {
		return new HsqlOrderRepository();
	}
		
}
