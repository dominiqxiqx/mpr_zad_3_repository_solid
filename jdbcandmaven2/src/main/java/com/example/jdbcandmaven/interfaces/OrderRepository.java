package com.example.jdbcandmaven.interfaces;

import java.util.List;
import com.example.jdbcandmaven.domain.Order;

public interface OrderRepository extends Respository<Order>{
	
	public List<Order> withLogin (String login);
	public List<Order> withItemName (String itemName);
}
