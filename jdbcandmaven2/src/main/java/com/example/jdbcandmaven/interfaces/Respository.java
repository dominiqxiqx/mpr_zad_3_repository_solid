package com.example.jdbcandmaven.interfaces;


public interface Respository <TEntity> {
	
	public TEntity withId(int id);
	public void add(TEntity entity);
	public void modify(TEntity entity);
	public void remove(TEntity entity);
}
