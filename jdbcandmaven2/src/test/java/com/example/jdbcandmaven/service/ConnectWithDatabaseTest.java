package com.example.jdbcandmaven.service;

import static org.junit.Assert.*;
import org.junit.Test;

public class ConnectWithDatabaseTest {

	private ConnectWithDatabase connectWithDatabase = new ConnectWithDatabase();
	
	@Test
	public void checkConnection() {
		assertNotNull(connectWithDatabase.getConnection());
	}
}
